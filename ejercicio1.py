#!/usr/bin/env python3
#! -*- coding:utf-8 -*-

lista = [] 
cantidad = int(input('ingrese sus numeros reales : '))
i = 1
suma = 0 
cuadrados = []
#se recorre hasta la cantidad primeramente dada	
for i in range(cantidad):

	numeros = float(input('numero# ' +  str(i+1) + ":"))
	lista.append(numeros)
	#agrega un ultimo digito a la lista
	i = i+1 
	suma += numeros
	#la suma
	cantidad += 1 
	cuadrados.append(numeros**2) #sacar el cuadrado de cada elemento
		 
#se imprime	
print ('mi promedio es', suma/numeros)
print ('los numeros al cuadrado es ', cuadrados)	

	
		
					

#Desarrolle un módulo llamada comprobador.py que contenga las siguientes funciones:
#Una función llamada promedio(lista), cuyo parámetro lista sea una lista de números reales (float)
#y que entregue el promedio de los números de dicha lista. Ejemplo
#promedio ([7.0 , 4.0 , 1.7])
#>> 6.0
#Una función llamada cuadrados y con un argumento de tipo lista (cuadrados(lista)), que entregue
#una lista con los cuadrados de los valores del parámetro lista.
#cuadrados ([2 , 3 , 4 , 5 , 10])
#>> [4 , 9 , 16 , 25 , 100]
#Una función llamada cuenta letras con un argumento de lista de strings, que al ser llamada el
#parámetro debe ser una lista de strings, y que devuelva es el string más largo.
#cuanta_letras ([ ’ hola ’ , ’ chao ’ , ’ paralelepipedo ’ , ’ mundo ’ ])
#>> ’ paralelepipedo 
